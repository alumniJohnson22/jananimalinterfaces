package com.m3.training.ufo;

import com.m3.training.interfaces.Flyable;

public class Fly implements Flyable {

	public Fly() {
	}

	@Override
	public String fly() {
		return "Bzzzz";
	}

}
