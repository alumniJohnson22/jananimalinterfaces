package com.m3.training.ufo;

import java.util.ArrayList;
import java.util.List;

import com.m3.training.interfaces.Flyable;

public class Driver {

	public static void main (String[] args) {
		Bird bird = new Bird();
		Hawk hawk = new Hawk();
		Fly fly = new Fly();
		Airplane airplane = new Airplane();
		Pegasus pegasus = new Pegasus();
		
		List<Flyable> listOfUFO = new ArrayList<>();
		listOfUFO.add(bird);
		listOfUFO.add(hawk);
		listOfUFO.add(fly);
		listOfUFO.add(airplane);
		listOfUFO.add(pegasus);
		
		for (Flyable ufo : listOfUFO) {
			System.out.println(ufo + " " + ufo.fly());
		}
	}

}
