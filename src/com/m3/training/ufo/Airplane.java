package com.m3.training.ufo;

import com.m3.training.interfaces.Flyable;

public class Airplane implements Flyable {

	@Override
	public String fly() {
		return "Motorized flight";
	}

}
