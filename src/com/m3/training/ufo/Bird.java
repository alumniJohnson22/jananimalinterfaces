package com.m3.training.ufo;

import com.m3.training.interfaces.Flyable;

public class Bird implements Flyable {

	public Bird() {
		super();
	}

	@Override
	public String fly() {
		return "Bird flying";
	}
	
	public String peck() {
		return "peck ... peck";
	}

}
