package com.m3.training.interfaces;

public interface Flyable {
		public default String fly() {
			return "Generic Flying";
		}
}
