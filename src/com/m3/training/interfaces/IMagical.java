package com.m3.training.interfaces;

public interface IMagical {
	
	public default String fly()
	{
		return "Magical Flying..";
	}
}
