package com.m3.training.interfaces;

public interface Aerodyne extends Flyable {

	public default String thrust()
	{
		return "I am pushing myself up";
	}
}
